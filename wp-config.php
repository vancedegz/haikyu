<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'tf-wpdev' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

if ( !defined('WP_CLI') ) {
    define( 'WP_SITEURL', $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] );
    define( 'WP_HOME',    $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] );
}



/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'R3g1TAuxG06AROcGOxDwnrB9ZkSug75t2NFUoD7lt2PsBxmwiwCdpn5D66uqtlvl' );
define( 'SECURE_AUTH_KEY',  '8TSg3Sa6RM6too4HAzfgY9WqTl8m2SFNg6NblRoHmiOxc8UxUsrLJygPcjT7TTeT' );
define( 'LOGGED_IN_KEY',    'HuYYpVi1SXsfY0LZrEHRlCjT9lcuNcAbrGr7AprobHl4DRbVSIZR1LkeT2GoewsD' );
define( 'NONCE_KEY',        'wJNeY0aCjpYNJFlgUYzqS6pdHswfc0QmUaI91u242Y49pJtgmKJKyRTtycp23WLo' );
define( 'AUTH_SALT',        'H6ohVujxk4NTWAvokMx430XW84qoBjsvVZfRMTK3TatvK1oV09CS0W3CV8Ykpjk4' );
define( 'SECURE_AUTH_SALT', 'QCDftcBfi4eoohqUibWj3mWOlwyWxioWwky50mgUL2oiGwrt1GQwPFuo4CTQ7Yu6' );
define( 'LOGGED_IN_SALT',   'tqllpfoj8yXqat5pu6cuoxkogaaLJv1YJq9wQRSIFzDeihuVROrL6cXQ73K1LbRb' );
define( 'NONCE_SALT',       'yRPqVmZkHfbNJK1EYHdQNXXWvJHGgyX0Ikyd5vbcC9aaJJP4QP8F8N5gzvge3wC5' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
